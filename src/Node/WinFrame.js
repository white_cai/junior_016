WinFrame = cc.Layer.extend({
	cur : 0,
	tip : [],
	lineMarign:10,
	posX: 0,
	posY: 0,
	open_flag: false,
	cell:null,
	ctor:function (parent,next,str,fileName) {
		this._super();
		parent.addChild(this,10000);
		this.setPosition(0, gg.height);
		if(next){
			//实验目的 原理 提示的提示框
			this.init(str,fileName);
			var tipbg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
			parent.addChild(tipbg,10, TAG_TIPSTARTBG);
			tipbg.setOpacity(0);
		}else{
			//按确定的提示框（选错）
			this.init2(str);
		}	
		//提示框后的背景层，透明度200

	},
	init:function (str,fileName){
		var bg = new cc.Sprite("#tip_frame_bg.png");
		bg.setPosition(cc.p(gg.width * 0.5 ,gg.height - 55 - bg.height * 0.5));
		this.addChild(bg,2);

		var frame = new cc.Sprite(fileName);
		frame.setPosition(cc.p(gg.width * 0.5 ,gg.height - 40 - frame.height * 0.5));
		this.addChild(frame,3);

		var close = new ButtonScale(this,"#tip_frame_close.png",this.callback);
		close.setPosition(cc.p(gg.width - 108 - close.width*0.5 , gg.height - 60 - close.height*0.5));
		close.setLocalZOrder(3);
		close.setTag(TAG_TIP_FRAME_CLOSE);

		var word = new cc.LabelTTF(str,"Arial",36);
		this.addChild(word,3);
		word.setAnchorPoint(0, 1);
		word.setPosition(cc.p(306,gg.height - 244 ));
		word.setColor(cc.color(111,65,9));
	},
	init2:function(str){
		var tipbg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
		ll.tip.addChild(tipbg,999, TAG_TIPSTARTBG);
		tipbg.setOpacity(0);

		var bg = new cc.Sprite("#button/lib_tip.png");
		bg.setPosition(cc.p(gg.width * 0.5 ,gg.height - 55 - bg.height * 0.5 +30));
		this.addChild(bg,2);		

		var close = new ButtonScale(this,"#button/lib_tip_close.png",this.callback);
		close.setPosition(cc.p(811,692 +30));
		close.setLocalZOrder(3);
		close.setTag(TAG_LIB_TIP_CLOSE);

		var word = new cc.LabelTTF(str,"Arial",36);
		bg.addChild(word,3);
		word.setAnchorPoint(0.5,0.5);
		word.setPosition(cc.p(bg.width*0.5,bg.height*0.5));
		word.setColor(cc.color(111,65,9));
	},
	open:function (){
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 200));		
		var move = cc.moveTo(0.3,cc.p(0, 0));
		var move2 = cc.moveTo(0.1,cc.p(0, 50));
		var move3 = cc.moveTo(0.1,cc.p(0, 0));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
	},
	close:function (){
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 0));
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		var sequence = cc.sequence(move);
		this.runAction(sequence);
		AngelListener.setEnable(true);
		this.open_flag = false;
	},
	open2:function (){
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 200));		
		var move = cc.moveTo(0.3,cc.p(0, 0-gg.height*0.25));
		var move2 = cc.moveTo(0.1,cc.p(0, 50-gg.height*0.25));
		var move3 = cc.moveTo(0.1,cc.p(0, 0-gg.height*0.25));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
	},
	close2:function (){
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 0));
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		var sequence = cc.sequence(move,cc.callFunc(function() {
			AngelListener.setEnable(true);
			this.open_flag = false;
			var bg = ll.tip.getChildByTag(TAG_TIPSTARTBG);
			bg.removeFromParent();
			this.removeFromParent();
		}, this));
		this.runAction(sequence);	
	},
	//首页面5个选择按钮
	buttonSetEnable:function(next){
		var leadButton = this.getParent().getChildByTag(TAG_LEAD);
		var realButton = this.getParent().getChildByTag(TAG_REAL);
		var purposeButton = this.getParent().getChildByTag(TAG_PURPOSE);
		var principleButton = this.getParent().getChildByTag(TAG_PRINCIPLE);
		var gameButton = this.getParent().getChildByTag(TAG_GAME);
		leadButton.setEnable(next);
		realButton.setEnable(next);
		purposeButton.setEnable(next);
		principleButton.setEnable(next);
		gameButton.setEnable(next);
	},
	//lib中取消 确定 药品库按钮 tip按钮
	buttonSetEnable2:function(next){
		var cancel = ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_CANCEL);
		var sure = ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_SURE);
		var tip = ll.tip.getChildByTag(TAG_TIP);
		var lib = ll.tool.getChildByTag(TAG_BUTTON_LIB);
		var back = ll.tip.getChildByTag(TAG_CLOSE);
		cancel.setEnable(next);
		sure.setEnable(next);
		tip.setEnable(next);
		lib.setEnable(next);
		back.setEnable(next);
	},
	isOpen:function (){
		return this.open_flag;
	},
	callback:function(p){
		switch (p.getTag()) {
		case TAG_TIP_FRAME_CLOSE:
			if(this.isOpen()){
				cc.log("关闭提示");
				gg.synch_l = false;
				this.close();
				this.buttonSetEnable(true);
			}
			break;
		case TAG_LIB_TIP_CLOSE:
			if(this.isOpen()){
				cc.log("关闭错误提示");
				gg.synch_l = false;
				this.close2();
				this.buttonSetEnable2(true);
			}
			break;

		default:
			break;
		}

	}
});