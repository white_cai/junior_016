var Button = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,zIndex,tag,normalImage, callback, back) {
		this._super(parent,normalImage, callback, back);
		this.setTag(tag);
		this.setLocalZOrder(zIndex);
		// 默认不能点击
		this.setEnable(false);
	},
	preCall:function(){
		// 隐藏箭头
		ll.tip.arr.out();
		this.deleteShow();
		this.setEnable(false);
		// 操作成功
		if((!ll.run.lib.isOpen()) && gg.scoreCheck){
			// 操作成功
			ll.tip.mdScore(10);
		}	
		if(!gg.errFlag){
			gg.oneSure ++;
		}
		ll.tip.score.setString("分数：" + gg.score);
		gg.errFlag = false;
		_.clever();
	},
	exeUnEnable:function(){
		// 操作失败
		if((!ll.run.lib.isOpen()) && gg.scoreCheck){
			// 操作成功
			ll.tip.mdScore(-3);
		}	
		gg.errFlag = true;
		gg.errorStep ++;
		_.error();
	},
	deleteShow:function(){
		if(ll.run == null){
			return;
		}
		var show = ll.run.getChildByTag(TAG_SHOW);
		if(show == null){
			return;
		}
		show.kill();
	}
})