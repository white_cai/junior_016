var TipLayer = cc.Layer.extend({
	scene:null,
	tip:null,
	time:null,
	score:null,
	flash:null,
	tip_frame:null,
	up_button:null,
	down_button:null,
	rotateAction:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 30);
		this.init();
	},
	init: function (){
		// 引导标记
		this.arr = new Arrow(this);
		this.addChild(this.arr, 100);

		this.tip = new Tip("", 840,gg.fontName,30);
		this.addChild(this.tip, 10);
		this.tip.setPosition(228, gg.height - 24);

		this.flash = new Flash("", gg.width * 0.6);
		this.addChild(this.flash, 10);
		this.flash.setPosition(300, 620);

		this.tip_frame= new TipFrame(this,"#help.png");	

		this.tipItem = new ButtonScale(this,"#button/tip.png",this.eventMenuCallback);
		this.tipItem.setTag(TAG_TIP);
		this.tipItem.setPosition(40 + this.tipItem.width*0.5,164 + this.tipItem.height * 0.5 );

		this.backItem = new ButtonScale(this,"#button/back.png",this.eventMenuCallback);
		this.backItem.setTag(TAG_CLOSE);
		this.backItem.setPosition(40 + this.backItem.width * 0.5, gg.height - 30 - this.backItem.height * 0.5);

		this.scoreLabel = new cc.LabelTTF("得分： ",gg.fontName, 30);
		this.scoreLabel.setPosition(gg.width - 124, gg.height - 70);
		this.scoreLabel.setAnchorPoint(1, 1);
		this.addChild(this.scoreLabel, 12);
		this.scoreLabel.setVisible(false);

		this.time = new cc.LabelTTF("00:00:00", gg.fontName, 30);
		this.score = new cc.LabelTTF("0分", gg.fontName, 30);
		this.time.setPosition(gg.width - 40, gg.height - 30);
		this.time.setAnchorPoint(1, 1);
		this.time.setColor(cc.color(255,255,255,0));
		this.score.setPosition(gg.width - 40, gg.height - 70);
		this.score.setAnchorPoint(1, 1);
		this.score.setColor(cc.color(255,255,255,0));	
		this.addChild(this.time, 12);
		this.addChild(this.score, 12);

		if(gg.teach_type == TAG_LEAD){
			this.score.setVisible(false);
			this.scoreLabel.setVisible(false);
		} else {
			this.tip.setVisible(false);
		}
		this.updateTime();
		this.schedule(this.updateTime, 1);

		this.tip_frame.loadSlide();
	},
	sayFlash:function(str){
		this.flash.doFlash(str);
	},
	updateTime:function(){
		var now = new Date();
		var ds = Math.round((now - gg.begin_time) / 1000);
		var hou = parseInt(ds/3600);
		var min = parseInt(ds/60);
		while (min>=60) {
			min=min-60;
		}
		var sec = ds%60;

		if(sec<=9){
			if(min<=9){
				if(hou<=9){
					this.time.setString("0"+hou+":0"+min+":0"+sec);
				}else{
					this.time.setString(hou+":0"+min+":0"+sec);
				}
			}else{
				if(hou<=9){
					this.time.setString("0"+hou+":"+min+":0"+sec);
				}else{
					this.time.setString(hou+":"+min+":0"+sec);
				}
			}
		}else{
			if(min<=9){
				if(hou<=9){
					this.time.setString("0"+hou+":0"+min+":"+sec);
				}else{
					this.time.setString(hou+":0"+min+":"+sec);
				}
			}else{
				if(hou<=9){
					this.time.setString("0"+hou+":"+min+":"+sec);
				}else{
					this.time.setString(hou+":"+min+":"+sec);
				}
			}
		}
	},
	mdScore:function(score){
		if(!score){
			return;
		}		
		gg.score += score;
		if(gg.score<=0){
			gg.score=0;
		}
		this.updateScore();
	},
	updateScore:function(){
		this.score.setString(gg.score + "分");
	},
	over:function(){
		this.unschedule(this.updateTime);
		gg.end_time = new Date();
	},
	eventMenuCallback: function(pSender) {
		switch (pSender.getTag()){
		case TAG_TIP:
			if(this.tip_frame.isOpen()){
				cc.log("关闭提示");
				gg.synch_l = false;
				gg.logo_onclick = true;
				this.tip_frame.close();
				this.tip_frame.libSetEnable(true);
			} else {
				cc.log("打开提示");
				gg.synch_l = true;
				gg.logo_onclick = false;
				this.tip_frame.refresh();
				this.tip_frame.open();
				this.tip_frame.libSetEnable(false);

				// 定位帮助信息
				var step = gg.flow.getStep();
				this.tip_frame.local(step);
				ll.tip.mdScore(-3);
			}
			break;
		case TAG_CLOSE:
			$.runScene(new StartScene());
			break;
		default:
			break;
		}
	}
});