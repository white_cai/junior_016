/**
 * 文字自带边框的实体
 */
ShowTip = cc.Node.extend({
	ctor:function(name,pos,next,time){
		this._super();
		ll.run.addChild(this, 100, TAG_SHOW);
		this.setCascadeOpacityEnabled(true);
		this.init(name,pos,next,time);
	},
	init:function(name,pos,next,time){
		var content = new cc.LabelTTF(name, gg.fontName, gg.fontSize);
		var mr = 10;
		var rect = content.getBoundingBoxToWorld();
		var bg = new cc.Scale9Sprite(res_start.show_tip);
		bg.setAnchorPoint(0, 0.5);
		bg.width = rect.width + mr * 5;
		bg.height = rect.height + mr * 5;
		bg.setPosition(pos );
		//rect.width * 0.5, rect.height * 0.5
		this.addChild(bg);
		bg.addChild(content,2);
		content.setColor(cc.color(38,41,52));
		content.setPosition(cc.p(bg.width*0.5,bg.height*0.5));

		if(next == null){
			next=false;
		}
		if(time == null){
			time=2;
		}
		if(next){
			this.scheduleOnce(this.kill, time);	
		}
	},
	kill:function(){
		var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
			this.removeFromParent(true);		
		}, this))
		this.runAction(seq);
	},
	fadein:function(){
		this.runAction(cc.fadeIn(0));
	},
	fadeout:function(){
		this.runAction(cc.fadeOut(0));
	}
})