/**
 * 火柴
 */
Match = Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,31);
		this.setTag(TAG_MATCH);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);
		//火柴盒
		this.box = new Button(this, 10, TAG_BOX, "#match.png",this.callback);
		this.box.setPosition(cc.p(gg.width*0.2 + 50 , gg.height*0.15));
		this.box.setScale(0.15);
		
		var match1 = new cc.Sprite("#match2.png");
		this.addChild(match1, 2,TAG_MATCH1);
		match1.setScale(0.3);
		match1.setPosition(cc.p(350,165));
		match1.setVisible(false);
		
		var match2 = new cc.Sprite("#match2.png");
		this.addChild(match2, 2,TAG_MATCH2);
		match2.setScale(0.3);
		match2.setPosition(cc.p(350,165));
		match2.setVisible(false);
	},	
	fire:function(){
		var fire = new cc.Sprite("#fire/fire1.png");
		this.addChild(fire,3,TAG_FIRE);
		fire.setRotation(-38);
		fire.setPosition(cc.p(446,700));
		
		var animFrames = [];
		for (var i = 1; i < 5; i++) {
			var str = "fire/fire" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.2,10);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		fire.runAction(seq);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_BOX:
			if(action == ACTION_DO1){
				var match1 = this.getChildByTag(TAG_MATCH1);
				match1.setVisible(true);
				var move1 = cc.moveTo(0.6,cc.p(392,139));
				var move2 = cc.moveTo(1, cc.p(478,689));
				var fadeout = cc.fadeOut(0);
				match1.runAction(cc.sequence(move1,cc.callFunc(function() {
					match1.setSpriteFrame("match1.png");
				}, this),move2,fadeout,cc.callFunc(function() {
					var left = this.getParent().getChildByTag(TAG_ELECTROLYSIS).getChildByTag(TAG_ELECTROLYSIS_LEFT);
					left.setSpriteFrame("left2.png");
					left.setPosition(cc.p(457,616));
					this.fire();
					var show = new ShowTip("左边玻璃管产生的气体可燃烧，并产生淡蓝色火焰\n说明负极产生的气体是氢气",cc.p(650,420));
				}, this),cc.delayTime(4),cc.callFunc(function() {
					var fire = this.getChildByTag(TAG_FIRE);
					fire.runAction(cc.fadeOut(2));
				}, this),cc.delayTime(1.5),cc.callFunc(function() {
					gg.flow.next();
				}, this)));
			}else if(action == ACTION_DO2){
				var match2 = this.getChildByTag(TAG_MATCH2);
				match2.setVisible(true);
				var move1 = cc.moveTo(0.6,cc.p(392,139));
				var move2 = cc.moveTo(0.3,cc.p(385,149));
				var move3 = cc.moveTo(1, cc.p(628,636));
				var fadeout = cc.fadeOut(0);
				match2.runAction(cc.sequence(move1,cc.callFunc(function() {
					match2.setSpriteFrame("match1.png");
				}, this),move2,cc.delayTime(0.5),cc.callFunc(function() {
					match2.setSpriteFrame("match3.png");
				}, this),cc.delayTime(0.5),move3,cc.callFunc(function() {
					var right = this.getParent().getChildByTag(TAG_ELECTROLYSIS).getChildByTag(TAG_ELECTROLYSIS_RIGHT);
					right.setSpriteFrame("right2.png");
					right.setPosition(cc.p(581,585));					
				}, this),cc.delayTime(0.2),cc.callFunc(function() {
					match2.setSpriteFrame("match4.png");
					var show = new ShowTip("右边玻璃管产生的气体可使带火星的火柴复燃\n说明正极产生的气体是氧气",cc.p(680,520),true,3.5);
				}, this),cc.delayTime(2),fadeout,cc.delayTime(2),cc.callFunc(function() {
					var show = new ShowTip("实验说明：\n水在通电的条件下，生成了氢气和氧气",cc.p(650,400));		
				}, this),cc.delayTime(2),cc.callFunc(function() {
					gg.flow.next();
					this.removeFromParent();
				}, this)));				
			}		
			break;
			default:
				break;
		}
	}
});
