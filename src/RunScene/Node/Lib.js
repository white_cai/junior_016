/**
 * 药品库
 */
Lib = cc.Sprite.extend({
	libArr: [],
	openFlag:true,
	doing:false,
	doright:true,
	rm : -5,
	ctor:function(p){
		this._super("#libshow.png");
		p.addChild(this,300);
		this.setTag(TAG_LIB);
		this.setCascadeOpacityEnabled(true);
		this.loadButton();
	},
	loadButton:function(){
		this.cancel = new ButtonScale(this,"#button/cancel.png",this.action);
		this.sure = new ButtonScale(this,"#button/sure.png",this.action);
		this.cancel.setPosition(cc.p(296 + this.cancel.width*0.5,  this.height - 446 - this.cancel.height*0.5));
		this.sure.setPosition(cc.p(526 + this.sure.width*0.5, this.height - 446 - this.sure.height*0.5 ));
		this.cancel.setTag(TAG_CANCEL);
		this.sure.setTag(TAG_SURE);
	},	
	loadBg:function(tags){
		this.setPosition(cc.p(140 + this.width*0.5, gg.height - 76 - this.height*0.5));
		this.checkArr = [];
		this.rightArr = [];
		var first = true;
		var firstPos = cc.p( 7 , this.height - 14);
		var prev = null;
		var num = 0;
		var j =1;
		for(var i in tags){
			var bg = new Button(this,2,TAG_LIB_BG+j,"#uncheck.png", this.callback);	
			if(gg.teach_type == TAG_REAL){
				//实战模式中，bg可点击
				bg.setEnable(true);	
			}					
			j++;
			bg.setAnchorPoint(0,1);
			var rel = this.getLibRel(tags[i]);
			var lib = new LibButton(bg,4, rel.tag, rel.img, this.callback,this);			
			bg.setCascadeOpacityEnabled(true);
			lib.setCascadeOpacityEnabled(true);
			lib.setPosition(cc.p(bg.width*0.5,bg.height*0.5+10));
			//第一个背景格子的坐标
			if(first){
				first = false;
				bg.setPosition(firstPos);
			} else {				
				//方格摆放顺序实际从左到右，第一行1 2 3 4 5  ，  第二行10 9 8 7 6
				if(num == 5){
					bg.down(prev, this.rm);
				}else if(num >5){
					bg.left(prev, this.rm);
				}else{
					bg.right(prev, this.rm);
				}				
			}
			if(rel.tag !== 30003 && rel.tag !== 30006 &&
					rel.tag !== 30008 && rel.tag !== 30009){
				//不包含正确的，即错误的方格传入数组
				this.checkArr.push(bg);
			}else{
				this.rightArr.push(bg);
			}
			prev = bg;
			num++;
		}		
	},
	getLibRel:function(tag){
		for(var i in libRelArr){
			if(libRelArr[i].tag == tag){
				return libRelArr[i];
			}
		}
	},
	callback:function(p){
		//检查仪器的父类（背景）是否点击
		if(!p.check){
			p.setSpriteFrame("check.png");
			p.check = true;
			//p的子类（仪器）的名称可见
			p.getChildren()[0].getChildByTag(TAG_CLIPPING).getChildByTag(TAG_WORD_BG).runAction(cc.moveBy(0.4,0,58));
		}else{
			p.setSpriteFrame("uncheck.png");
			p.check = false;
			p.getChildren()[0].getChildByTag(TAG_CLIPPING).getChildByTag(TAG_WORD_BG).runAction(cc.moveBy(0.4,0,-58));
		}	
		p.setEnable(true);	
		
		switch (p.getTag()) {
		case TAG_LIB_BG3:
		case TAG_LIB_BG6:
		case TAG_LIB_BG8:
		case TAG_LIB_BG9:
			if(!p.click){
				p.click=true;
				gg.flow.next();
			}
			break;
		default:
			break;
		}

	},
	action:function(p){
		switch(p.getTag()){
		case TAG_SURE:
			if(this.doright){
				var max = 0;
				//错误仪器中是否被选中，任意被选中，即错
				for(var i  in this.checkArr){						
					if(this.checkArr[i].check  ){
						this.show = new WinFrame(ll.tip,false,"仪器选择错误！");
						this.show.open2();
						this.show.buttonSetEnable2(false);
						ll.tip.mdScore(-3);
						return;
					}
				}
				//正确仪器中是否被选中，任意被选中，即错
				for(var i  in this.rightArr){
					if(this.rightArr[i].check){
						max++;																			
					}								
				}
				if(max == 0){
					if(ll.run.getChildByTag(TAG_SHOW)==null){
						this.show = new WinFrame(ll.tip,false,"请选择仪器！");
						this.show.open2();
						this.show.buttonSetEnable2(false);
						return;
					}
				}else if(max == 4){
					gg.flow.next();
					this.doright=false;		
					this.close();
					this.checkVisible(true);
					ll.tip.mdScore(10);
					gg.scoreCheck = true;
					p.setEnable(false);
				}else{
					if(ll.run.getChildByTag(TAG_SHOW)==null){
						this.show = new WinFrame(ll.tip,false,"仪器选择不足！");
						this.show.open2();
						this.show.buttonSetEnable2(false);
						ll.tip.mdScore(-3);	
						return;
					}				
				}				
			}else{
				this.show = new WinFrame(ll.tip,false,"仪器已选择！");
				this.show.open2();
				this.show.buttonSetEnable2(false);
			}
			break;
		case TAG_CANCEL:
			this.close();
			ll.tip.arr.fadein();
			break;
		}	
	},
	isOpen:function(){
		return this.openFlag; 
	},
	open:function(){
		if(this.openFlag || this.doing){
			return;
		}
		this.checkVisible(false);
		this.sure.setEnable(true);
		this.doing = true;
		var fadein = cc.fadeIn(0.4);
		ll.tip.scoreLabel.setVisible(false);
		var func = cc.callFunc(function(){			
			this.openFlag = true;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 显示箭头
					gg.flow.location();
				}
			}
		}, this);
		var seq = cc.sequence(fadein,func);
		this.runAction(seq);
	},
	close:function(){
		if(!this.openFlag || this.doing){
			return;
		}
		if(!this.doright){
			this.checkVisible(true);
		}	
		this.doing = true;
		var fadeout = cc.fadeOut(0.4);
		if(gg.teach_type == TAG_REAL){
			ll.tip.scoreLabel.setVisible(true);
		}	
		var func = cc.callFunc(function(){
			this.openFlag = false;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 隐藏箭头
					//ll.tip.arr.out();
					ll.tip.arr.pos(ll.tool.getChildByTag(TAG_BUTTON_LIB));
				}
			}
		}, this);
		var seq = cc.sequence(fadeout,func);
		this.runAction(seq);
	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];		
		var electrolysis = ll.run.getChildByTag(TAG_ELECTROLYSIS);
		var match = ll.run.getChildByTag(TAG_MATCH);
		var show = ll.run.getChildByTag(TAG_SHOW);
		checkVisible.push(electrolysis,match,show);
		//动作暂停，恢复
		var actionPause = [];
		var off = electrolysis.getChildByTag(TAG_OFF);
		var on = electrolysis.getChildByTag(TAG_ON);
		var dc_v = electrolysis.getChildByTag(TAG_BODY).getChildByTag(TAG_DC_V);
		var match1 =match.getChildByTag(TAG_MATCH1);
		var match2 =match.getChildByTag(TAG_MATCH2);
		actionPause.push(off,on,dc_v,match1,match2);
		
		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);
				for(var i in actionPause){
					if(actionPause[i] !== null){
						if(next){
							actionPause[i].resume();
						}else{
							actionPause[i].pause();
						}
					}
				}
			}			
		}
	}
});
TAG_LABEL=30020;
TAG_WORD_BG=30021;

TAG_LIB_MIN = 30000;

TAG_LIB_AMPULLA = 30001;
TAG_LIB_BEAKER=30002;
TAG_LIB_WIRE=30003;
TAG_LIB_CONICAL=30004;
TAG_LIB_CYLINDER=30005;
TAG_LIB_DC=30006;
TAG_LIB_PIPE=30007;
TAG_LIB_ELECTROLYSERS=30008;
TAG_LIB_MATCH=30009;
TAG_LIB_VOLUMETRIC=30010;

TAG_LIB_BG=30050;
TAG_LIB_BG1=30051;
TAG_LIB_BG2=30052;
TAG_LIB_BG3=30053;
TAG_LIB_BG4=30054;
TAG_LIB_BG5=30055;
TAG_LIB_BG6=30056;
TAG_LIB_BG7=30057;
TAG_LIB_BG8=30058;
TAG_LIB_BG9=30059;
TAG_LIB_BG10=30060;

TAG_CANCEL=30081;
TAG_SURE=30082;

libRelArr = [
             {tag:TAG_LIB_MIN, name:""},  
             {tag:TAG_LIB_AMPULLA, name:"玻璃瓶",img:"#ampulla.png"},
             {tag:TAG_LIB_BEAKER,name:"烧杯",img:"#beaker.png"},
             {tag:TAG_LIB_WIRE,name:"导线",img:"#wire.png"},
             {tag:TAG_LIB_CONICAL,name:"锥形瓶",img:"#conical.png"},
             {tag:TAG_LIB_CYLINDER,name:"量筒",img:"#cylinder.png"},
             {tag:TAG_LIB_DC, name:"低压直流电源",img:"#DC.png"},
             {tag:TAG_LIB_PIPE,name:"冷凝管",img:"#pipe.png"},
             {tag:TAG_LIB_ELECTROLYSERS,name:"水电解器",img:"#electrolysers.png"},
             {tag:TAG_LIB_MATCH,name:"火柴",img:"#match.png"},
             {tag:TAG_LIB_VOLUMETRIC,name:"烧瓶",img:"#volumetric.png"},
             ];
