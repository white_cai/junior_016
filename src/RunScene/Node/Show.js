Show = cc.Sprite.extend({
	ctor:function(name){
		this._super(name);
		this.setAnchorPoint(0, 0.5);
		if(ll.run != null){
			ll.run.addChild(this, 100, TAG_SHOW);
		}
	},
	kill:function(){
		var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
			this.removeFromParent(true);
		},this));
		this.runAction(seq);
	}
})