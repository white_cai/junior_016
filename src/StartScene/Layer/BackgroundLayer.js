var StartBackgroundLayer = cc.LayerColor.extend({
	zindex:5,
    ctor:function () {
        this._super();
        this.loadBg();
        this.loadTitle();
        this.loadWosai();
        return true;
    },
    loadBg : function(){
    	var start_bg = new cc.Sprite(res_start.start_bg);
    	start_bg.setPosition(gg.width * 0.5, gg.height * 0.5);
    	this.addChild(start_bg,1);
    },
    loadTitle:function(){
    	var title = new cc.LabelTTF("水的电解","Arial","80");
    	title.setPosition(gg.width * 0.5, gg.height - 160 - title.height * 0.5);
    	this.addChild(title,2);
    },
    loadWosai:function(){
    	var wosai  = new cc.Sprite(res_start.wosai);
    	wosai.setPosition(wosai.width * 0.5 + 40, gg.height -40 - wosai.height * 0.5);
    	this.addChild(wosai,2);
    }
});