TAG_LEAD = 1001;
TAG_REAL = 1002;
TAG_PURPOSE = 1003;
TAG_PRINCIPLE = 1004;
TAG_GAME = 1005;
TAG_INDEX = 1006;
TAG_ABOUT = 1007;
TAG_TIPSTARTBG = 1008;

var StartMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		// 加载菜单按钮
		this.loadButton();
		// 加载主页
		this.loadIndex();
		// 加载关于
		this.loadAbout();
		//加载目的原理窗口
		this.loadWin();
		return true;
	},
	loadWin:function(){
		this.purpose_win = new WinFrame(this,true,"1)  认识水是由氢、氧两种元素组成的；\n2)  验证水在通电条件下，生成氢气和\n     氧气。","#purpose.png");
		this.principle_win = new WinFrame(this,true,"1)  水是由氢、氧两种元素组成；\n2)  水==（通电直流）氢气 + 氧气。","#principle.png");
	},
	loadButton:function(){
		this.leadButton = new ButtonScale(this,"#leadButton.png",this.callback);		
		this.leadButton.setPosition(cc.p(400 + this.leadButton.width * 0.5, 244 + this.leadButton.height * 0.5));
		this.leadButton.setTag(TAG_LEAD);
		this.leadButton.setLocalZOrder(3);

		this.realButton = new ButtonScale(this,"#realButton.png",this.callback);		
		this.realButton.setPosition(cc.p(400 + this.leadButton.width + 84 + this.realButton.width * 0.5, 244 + this.realButton.height * 0.5));
		this.realButton.setTag(TAG_REAL);
		this.realButton.setLocalZOrder(3);

		this.purposeButton = new ButtonScale(this,"#purposeButton.png",this.callback);
		this.purposeButton.setPosition(cc.p(238 + this.purposeButton.width * 0.5 , 125 + this.purposeButton.height * 0.5));
		this.purposeButton.setTag(TAG_PURPOSE);
		this.purposeButton.setLocalZOrder(3);

		this.principleButton = new ButtonScale(this,"#principleButton.png",this.callback);
		this.principleButton.setPosition(cc.p(238 + this.purposeButton.width + 116 + this.principleButton.width * 0.5, 125 + this.principleButton.height * 0.5));	
		this.principleButton.setTag(TAG_PRINCIPLE);
		this.principleButton.setLocalZOrder(3);

		this.gameButton = new ButtonScale(this,"#gameButton.png",this.callback);
		this.gameButton.setPosition(cc.p(238 + this.purposeButton.width + 116 + this.principleButton.width + 116 + this.gameButton.width * 0.5, 125 + this.gameButton.height * 0.5));
		this.gameButton.setTag(TAG_GAME);
		this.gameButton.setLocalZOrder(3);
	},
	loadIndex:function(){
		this.indexButton = new ButtonScale(this,"#indexButton.png",this.callback);
		this.indexButton.setPosition(cc.p(20 + this.indexButton.width * 0.5, 26 + this.indexButton.height * 0.5));
		this.indexButton.setTag(TAG_INDEX);
		this.indexButton.setLocalZOrder(5);
	},
	loadAbout:function(){
		this.aboutButton = new ButtonScale(this,"#aboutButton.png",this.callback);
		this.aboutButton.setPosition(cc.p(gg.width - 20 - this.aboutButton.width * 0.5, 26 + this.aboutButton.height * 0.5));
		this.aboutButton.setTag(TAG_ABOUT);
		this.aboutButton.setLocalZOrder(5);
	},
	cWin:function(name){
		if(name == null || name == ""){
			this.win.runAction(cc.fadeOut(0.15));	
			return;
		}
		var seq = cc.sequence(cc.fadeOut(0.15),cc.callFunc(function(){
			this.win.setSpriteFrame(name);
		}, this),cc.fadeIn(0.15));
		this.win.runAction(seq);
	},	
	callback:function(pSend){
		switch(pSend.getTag()){
		case TAG_INDEX:
			cc.log("结束");
			if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
				history.go(-1);
			} else if(cc.sys.os == "IOS"){
			} else if(cc.sys.os == "Android"
				|| cc.sys.os == "Windows"){
				cc.director.end();		
			}
			break;
		case TAG_ABOUT:
			this.runNext(pSend.getTag());
			break;
		case TAG_LEAD:
			cc.log("进入引导模式");
			gg.teach_type = TAG_LEAD;
			this.runNext(pSend.getTag());
			break;
		case TAG_REAL:
			cc.log("进入实战模式");
			gg.teach_type = TAG_REAL;
			this.runNext(pSend.getTag());
			break;
		case TAG_GAME:
			cc.log("进入小游戏模式");
			this.runNext(pSend.getTag());
			break;
		case TAG_PURPOSE:
			cc.log("实验目的");
			if(this.purpose_win.isOpen()){
				gg.synch_l = false;
				gg.logo_onclick = true;
				this.purpose_win.close();				
			} else {
				gg.synch_l = true;
				gg.logo_onclick = false;
				this.purpose_win.open();
				this.purpose_win.buttonSetEnable(false);
			}
			break;
		case TAG_PRINCIPLE:
			cc.log("实验原理");
			if(this.principle_win.isOpen()){
				gg.synch_l = false;
				gg.logo_onclick = true;
				this.principle_win.close();
			} else {				
				gg.synch_l = true;
				gg.logo_onclick = false;
				this.principle_win.open();
				this.principle_win.buttonSetEnable(false);
			}
			break;
		}
	},
	runNext:function(tag){
		switch(tag){
		case TAG_LEAD:
		case TAG_REAL:
			if(gg.run_load){
				$.runScene(new RunScene());
				return;
			}
			break;
		case TAG_GAME:
			if(gg.game_load){
				$.runScene(new GameScene());
				return;
			}
			break;
		case TAG_ABOUT:
			if(gg.about_load){
				$.runScene(new AboutScene());
				return;
			}
			break;
		}
		if(this.tip == null){
			this.tip = new cc.LabelTTF("加载中，请稍候再试",gg.fontName,gg.fontSize);
			this.tip.setColor(cc.color(0,0,0,0));
			this.tip.setPosition(gg.width * 0.5, gg.height * 0.5)
			this.addChild(this.tip);
		} else {
			this.tip.setVisible(true);
		}
		this.scheduleOnce(function(){
			this.tip.setVisible(false);	
		}.bind(this), 1.5);
	}
});
