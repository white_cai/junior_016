teach_flow = 
	[
	 {
		 tip:"选择导线",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择火柴",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择水电解器",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择低压直流电源",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG6,
		      ],
		      canClick:true
	 },	
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"打开直流电源的开关",
		 tag:[TAG_ELECTROLYSIS,
		      TAG_BODY,
		      TAG_DC_ONOFF
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"旋转按钮至12V，观察实验现象",
		 tag:[TAG_ELECTROLYSIS,
		      TAG_BODY,
		      TAG_DC_V
		      ]
	 },
	 {
		 tip:"关闭直流电源的开关",
		 tag:[TAG_ELECTROLYSIS,
		      TAG_BODY,
		      TAG_DC_ONOFF
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"取出火柴点燃，并靠近左边玻璃管嘴，并分析实验",
		 tag:[TAG_MATCH,
		      TAG_BOX
		      ],action:ACTION_DO1
	 },{
		 tip:"将带火星的火柴靠近右边玻璃管嘴，并分析实验",
		 tag:[TAG_MATCH,
		      TAG_BOX
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]